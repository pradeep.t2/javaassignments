package com.AssignmentQ2;

public class Area extends Shap {

	@Override
	public void RectangleArea(int len, int breth) {
		System.out.println("RectangleArea values:"+len*breth);
	}

	@Override
	public void SquareArea(int side) {
		System.out.println("SquareArea values:"+side*side);
	}

	@Override
	public void CircleArea(double radius) {
		System.out.println("radius values:"+radius*radius%3.14);
	}
	public static void main(String[] args) {
		Area a=new Area();
		a.RectangleArea(10, 20);
		a.SquareArea(10);
		a.CircleArea(90);
		
	}

}
